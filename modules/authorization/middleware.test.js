const { container } = require('../../app');

describe('auth middleware test', () => {
  const authMiddleware = container.resolve('authMiddleware');

  test('should throw a unauthorized error', async () => {
    const ctx = { request: {}, headers: {}, query: {} };
    try {
      await authMiddleware.jwtAuthz(ctx).call(null, ctx);
    } catch (e)  {
      expect(e.status).toBeTruthy();
      expect(e.message).toBeTruthy();
      expect(e.status).toEqual(401);
      expect(e.message).toEqual('Missing authorization token');
    }
  });

  test('should test query param validation and fail when requesting keys', async () => {
    const ctx = { request: {}, headers: {}, query: { accessToken: 'fake'} };
    try {
      await authMiddleware.jwtAuthz(ctx).call(null, ctx);
    } catch (e)  {
      expect(e.status).toBeTruthy();
      expect(e.message).toBeTruthy();
      expect(e.status).toEqual(503);
      expect(e.message).toEqual('Error fetching JWKS');
    }
  });

  test('should test header param validation and fail when requesting keys', async () => {
    const ctx = { request: {}, headers: { authorization: 'fake' }, query: {} };
    try {
      await authMiddleware.jwtAuthz(ctx).call(null, ctx);
    } catch (e)  {
      expect(e.status).toBeTruthy();
      expect(e.message).toBeTruthy();
      expect(e.status).toEqual(503);
      expect(e.message).toEqual('Error fetching JWKS');
    }
  });
});
