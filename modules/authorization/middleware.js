module.exports = ({ logger, authService, errors }) => {
  const { UnauthorizedError } = errors;
  const log = logger('module:authorization');

  return {
    jwtAuthz: (expectedScopes = []) => async (ctx, next) => {
      let token = null;
      if (ctx.headers.authorization) {
        const authorization = ctx.headers.authorization.split(' ');
        token = authorization.length > 1 ? authorization[1] : authorization[0];
      } else {
        token = ctx.query.accessToken;
      }

      if (!token) throw new UnauthorizedError('Missing authorization token');

      ctx.user = await authService.handleJWTAuthz(token, expectedScopes);
      return next();
    },
  };
};
