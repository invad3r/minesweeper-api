const fetch = require('node-fetch');
const { JWK, JWT } = require('@panva/jose');

module.exports = ({ config, errors, logger }) => {
  const log = logger('module:authorization');
  const { UnauthorizedError, ForbiddenError, ServiceUnavailableError } = errors;

  return {
    /**
    * @name handleJWTAuthz
    * @description validate signature and scopes of a JWT token
    * @param {string} token JWT token
    * @param {array} expectedScopes Optional, array of expected scopes (strings)
    * @returns {promise} returns a promise with a decoded payload
    */
    async handleJWTAuthz (token, expectedScopes = []) {
      const { uri } = config.get('jwks');
      const keys = await this.fetchKeys(uri);
      let decoded;

      // validate token signature
      try {
        decoded = JWT.decode(token);
        const key = JWK.asKey(keys.find(k => k.kid === decoded.aud.split(':')[0]))
        const isValid = JWT.verify(token, key);
      } catch (e) {
        log.log(e.toString(), 'error');
        throw new UnauthorizedError(`Invalid or expired token`);
      }

      // validate token scope
      if (expectedScopes.length) {
        const allowed = expectedScopes.some(e => decoded.scope.includes(e));
        if (!allowed) throw new ForbiddenError('Insufficient scope');
      }

      return { id: decoded.usr };
    },

    async fetchKeys (uri) {
      let keys;
      try {
        const response = await fetch(uri);
        if (response.status !== 200) {
          throw new Error(`Error fetching JWKS keys`);
        }
        const payload = await response.json();
        keys = payload.keys;
      } catch (error) {
        log.log(error.toString(), 'error');
        throw new ServiceUnavailableError(`Error fetching JWKS`);
      }
      return keys;
    },
  };
};
