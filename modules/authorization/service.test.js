const { container } = require('../../app');

const invitationJWT = 'eyJraWQiOiJtaW5lc3dlZXBlciIsImFsZyI6IlBTMjU2In0.eyJ1c3IiOiJ0ZXN0I2EyY2I0ZDBiIiwic3ViIjoiYTJjYjRkMGItMGU3Zi01YjBmLThkN2UtM2YzMzA4OTVhZGYyIiwiaXNzIjoiaW52aXRhdGlvbjphcGk6ZGV2IiwiaWF0IjoxNTY3Nzg3ODUxLCJleHAiOjEzMTE2Nzc4Nzg1MSwiYXVkIjoibWluZXN3ZWVwZXI6YXBpOmRldiIsInNjb3BlIjpbImdhbWU6Y3JlYXRlIiwiZ2FtZTpmZXRjaCJdfQ.pLv9-PTaRMICW4e4U_ta0nbhPls4gxaXIZV2JtCRbp4GM3z3nqU6FYd2KZejbDRsRQpwAJpa_tvnMC_xtPcjN56K7BG_i4peiKRbS4uhFOX1SLp4-uK1cmDbJYYBt8W9E3slUm4EfrVTAuXfi272KZvRAw3PP3ZB9r_y89jss5Nc5-IahGZB8FeOnEvc_7QRSeOGr2IIwbYCNbEEgTq4Nm5wRU5tBR668jUij4sR1v13fNT67xnhkjm19YQvgvpPd8COQ3w5lqmkdCALramEXANyOiiqzuHpuelzHsME5HbAKEAcWqYIuoZ6MRsSmxJ8__LRu3ia7npl2cDJ508BKQ';

const invalidJWT = 'eyJraWQiOiJtaW5lc3dlZXBlciIsImFsZyI6IlBTMjU2In0.eyJ1c3IiOiJ0ZXN0I2EyY2I0ZDBiIiwic3ViIjoiYTJjYjRkMGItMGU3Zi01YjBmLThkN2UtM2YzMzA4OTVhZGYyIiwiaXNzIjoiaW52aXRhdGlvbjphcGk6ZGV2IiwiaWF0IjoxNTY3Nzg4MTgzLCJleHAiOjEzMTE2Nzc4ODE4NCwiYXVkIjoibWluZXN3ZWVwZXI6YXBpOmRldiIsInNjb3BlIjpbImdhbWU6Y3JlYXRlIiwiZ2FtZTpmZXRjaCJdfQ.mGNHwPVcKeCdwloKzdXpkT9c15T_MkSljnCKRDEv_y31Nh39ugWkZ2mcScA14OsOmslz24-PP9E89eDjokiuElylXy-2awc3v3JSYRtzooD8ttlrBu6FDCDEPzj_wnL1RSYL4W7WAjhirhRcr8UBF9WR4X1vXjQARIynDOsqUxeO1_mKahvhR3H0zTF6Gp0Z_5EI8bfq3Z680U0PiUfwZ7C_tLesCMCeDMdqAsvN8USrLxVXhQ_gSWPD3VptcyrXDb04jzyGW0MgwqTd-LOx_pcY1zra1k_GBmC0utUq3lnl0SHfq05vQ91mf9EncS4leA4X07q3KYShgMsVB7InmA';

const mockedUserId = 'test#a2cb4d0b';

const mockedKeystore = {
  keys: [{
    e: 'AQAB',
    n: '3TfQ9wux8Lf6xspb9s5MND9XzoK495ELWRf_OGmZlZPA1x3_2WrBjooVxpZ9hoKm_dJyXtBcp7GEbEd85AVzrqA1nGoc9b1_ylROe4SvbUV7Eo1viouC5sz50vOkLgPkISA5vtayFRQlZ1hma4K6qls9yjb5XgPl6XMWGCzHBgWgdW4hGRH2V7mIGcyqMrhMzUL2SjqwRZdzSPFNn3CU2N_R_re1u9DwQNkz-LNhV45H9-WqLBxz5J0upciL7_njcJMHR3HoK4GWUxahxepkUnyXNttC5qz-w__18L2_qwb3N746bpvrFJErKFA-RxKeTS9Q3_E0hI_B7uAVnxvbgw',
    kty: 'RSA',
    kid: 'minesweeper'
  }]
};

describe('auth service test', () => {
  const authService = container.resolve('authService');
  authService.fetchKeys = (uri) => mockedKeystore.keys;

  test('should return a valid user object', async () => {
    const user = await authService.handleJWTAuthz(invitationJWT);
    expect(user.id).toBeTruthy();
    expect(user.id).toEqual(mockedUserId);
  });

  test('should throw a unauthorized error', async () => {
    try {
    const user = await authService.handleJWTAuthz(invalidJWT);
  } catch (e) {
    expect(e.status).toBeTruthy();
    expect(e.message).toBeTruthy();
    expect(e.status).toEqual(401);
    expect(e.message).toEqual('Invalid or expired token');
  }
  });

  test('should throw a forbidden error', async () => {
    try {
      await authService.handleJWTAuthz(invitationJWT, ['fake:scope']);
    } catch (e) {
      expect(e.status).toBeTruthy();
      expect(e.message).toBeTruthy();
      expect(e.status).toEqual(403);
      expect(e.message).toEqual('Insufficient scope');
    }
  });
});
