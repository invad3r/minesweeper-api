const mongoose = require('mongoose');
const { MongoMemoryServer } = require('mongodb-memory-server');
const Game = require('./models/game')


module.exports = ({ config, logger }) => {
  const log = logger('module:mongo-db');
  const { uri } = config.get('mongo');

  try {
    const mongod = new MongoMemoryServer();
    mongod.getConnectionString().then((uri) => {
      mongoose.connect(uri, { useNewUrlParser: true })
      log.log('successfully connected to in memory mongo db')
    });
  } catch(e) {
    log(e, 'error');
    throw e;
  }

  return { Game };
};
