const mongoose = require('mongoose');

const { Schema } = mongoose;
const GameSchema = new Schema({
  usr: {
    type: String,
    required: [true, 'user name is required'],
  },
  data: {
    type: Schema.Types.Mixed,
  },
  ts : { type : Date, default: Date.now }
});

module.exports = mongoose.model('game', GameSchema, 'games');
