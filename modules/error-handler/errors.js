const util = require('util');

function ServiceUnavailableError(message) {
  Error.captureStackTrace(this, this.constructor);
  this.name = this.constructor.name;
  this.message = message;
  this.code = 0;
  this.status = 503;
}

function InternalError(message) {
  Error.captureStackTrace(this, this.constructor);
  this.name = this.constructor.name;
  this.message = message;
  this.code = 0;
  this.status = 500;
}

function NotFoundError(message) {
  Error.captureStackTrace(this, this.constructor);
  this.name = this.constructor.name;
  this.message = message;
  this.code = 0;
  this.status = 404;
}

function UnauthorizedError(message) {
  Error.captureStackTrace(this, this.constructor);
  this.name = this.constructor.name;
  this.message = message;
  this.code = 0;
  this.status = 401;
}

function ForbiddenError(message) {
  Error.captureStackTrace(this, this.constructor);
  this.name = this.constructor.name;
  this.message = message;
  this.code = 0;
  this.status = 403;
}

function BadRequestError(message) {
  Error.captureStackTrace(this, this.constructor);
  this.name = this.constructor.name;
  this.message = message;
  this.code = 0;
  this.status = 400;
}

util.inherits(NotFoundError, Error);
util.inherits(UnauthorizedError, Error);
util.inherits(ForbiddenError, Error);
util.inherits(InternalError, Error);
util.inherits(BadRequestError, Error);
util.inherits(ServiceUnavailableError, Error);


module.exports = {
  NotFoundError,
  UnauthorizedError,
  ForbiddenError,
  InternalError,
  BadRequestError,
  ServiceUnavailableError,
};
