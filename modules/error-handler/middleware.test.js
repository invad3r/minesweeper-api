const Koa = require('koa');
const Router = require('koa-router');
const request = require('supertest');
const { container } = require('../../app');

describe('error handler test', () => {
  const errors = container.resolve('errors');
  const router = new Router();
  let SERVER;
  let AGENT;
  let APP;

  beforeAll(async () => {
    APP = new Koa();
    APP.use(container.resolve('errorMiddleware'));
    SERVER = APP.listen(9000);
    AGENT = request(SERVER);
  });

  afterAll(async () => {
    SERVER.close();
    await container.dispose();
  });

  test('should throw an unhandled error', async () => {
    router.get('/unhandled', () => { throw new Error('My Test Error'); });
    APP.use(router.routes());
    const response = await AGENT.get('/unhandled').send();
    expect(response.status).toEqual(500);
    expect(response.body.message).toEqual('My Test Error');
    expect(response.type).toEqual('application/json');
  });

  test('should throw an internal custom error', async () => {
    router.get('/internal', () => { throw new errors.InternalError('Internal Server Error'); });
    APP.use(router.routes());
    const response = await AGENT.get('/internal').send();
    expect(response.status).toEqual(500);
    expect(response.body.message).toEqual('Internal Server Error');
    expect(response.type).toEqual('application/json');
  });
});
