const Koa = require('koa');
const mount = require('koa-mount');

function handleValidationError(opts, err) {
  const message = Object.keys(err.errors).map(key => err.errors[key].message);
  return {
    status: 400,
    message: message ? message.join(', ') : 'Bad Request',
  };
}

function defaultHandler(opts, err) {
  if (opts.throw) {
    throw err;
  }
  return {
    status: err.statusCode || err.status || 500,
    message: err.message || 'Internal Server Error',
  };
}

module.exports = ({ config, logger }) => {
  const opts = config.get('errorHandler');
  const log = logger('module:api');
  const app = new Koa();

  app.use(async (ctx, next) => {
    try {
      await next();
    } catch (err) {
      let error;
      // will only respond with JSON
      switch (err.name) {
        case 'SequelizeValidationError':
          error = handleValidationError(opts, err);
          break;
        default:
          error = defaultHandler(opts, err);
      }

      ctx.status = error.status;
      ctx.body = { message: error.message };
      if (opts.logs) log.log(`error: ${err}`, 'error');
    }
  });

  return mount('/', app);
};
