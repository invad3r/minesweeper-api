const Koa = require('koa');
const mount = require('koa-mount');


module.exports = () => {
  const app = new Koa();

  app.use(async (ctx, next) => {
    const start = Date.now();
    await next();
    const ms = Date.now() - start;
    ctx.set('X-Response-Time', `${ms}ms`);
  });

  return mount('/', app);
};
