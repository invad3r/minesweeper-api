const Router = require('koa-router');

module.exports = ({ logger, errors, authMiddleware, minesweeperService }) => {
  const { BadRequestError, InternalError, NotFoundError } = errors;
  const { jwtAuthz } = authMiddleware;
  const log = logger('module:minesweeper');
  const router = new Router();

  router.post('/minesweeper/game', jwtAuthz(['game:create']), async (ctx) => {
    const { user } = ctx;
    const { data } = ctx.request.body;;
    let game;

    if (!data) throw new BadRequestError('Missing game data');

    try {
      game = await minesweeperService.newGame(user, data);
    } catch (e) {
      log.log(e, 'error')
      throw new InternalError('Sorry, that was unexpected...');
    }

    return ctx.ok(game);
  });

  router.get('/minesweeper/game', jwtAuthz(['game:fetch']), async (ctx) => {
    const { user } = ctx;
    let games;

    try {
      games = await minesweeperService.fetchGames(user);
    } catch (e) {
      log.log(e, 'error')
      throw new InternalError('Sorry, that was unexpected...');
    }

    return ctx.ok(games);
  });

  return router.routes();
};
