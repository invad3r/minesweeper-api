module.exports = ({ config, errors, logger, mongo }) => {
  const { Game } = mongo;
  return {
    newGame: async(user, data) => {
      const game = await new Game({ usr: user.id, data }).save();
      return game;
    },

    fetchGames: async(user) => {
      const games = await Game.find({ usr: user.id });
      return { games };
    },
  };
};
