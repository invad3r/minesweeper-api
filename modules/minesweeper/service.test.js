const { container } = require('../../app');

const mockedUserId = 'test#a2cb4d0b';

describe('minesweeper service test', () => {
  const minesweeperService = container.resolve('minesweeperService');

  test('should retun a new game from the DB', async () => {
    const game = await minesweeperService.newGame({id: mockedUserId}, { game: 'fake-data' });
    expect(game._id).toBeTruthy();
  });

  test('should return an array of games from the DB', async () => {
    const { games } = await minesweeperService.fetchGames({id: mockedUserId});
    expect(games[0]).toBeTruthy();
    expect(games[0]._id).toBeTruthy();
  });
});
