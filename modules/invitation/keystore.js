const { JWKS } = require('@panva/jose');

module.exports = class Keystore {
  constructor({ config }) {
    const { keys } = config.get('keystore');
    this.keystore = new JWKS.KeyStore();
    keys.forEach((kid) => {
      this.keystore.generateSync("RSA", 2048, { kid });
    });
  }

  async getJWKS() {
    return this.keystore.toJWKS();
  }

  async getKey(kid) {
    return this.keystore.get({ kid });
  }
}
