const routes = require('./routes');
const service = require('./service');
const keystore = require('./keystore');

module.exports = { routes, service, keystore };
