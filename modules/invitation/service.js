const uuidv5 = require('uuid/v5');
const { JWK, JWT } = require('@panva/jose');

module.exports = ({ config, errors, logger, keystore }) => {
  const { BadRequestError } = errors;

  return {
    /**
    * @name newInvitation
    * @description create a new invitation token
    * @param {string} username username
    * @param {string} service name of the requested service
    * @returns {promise} returns a signed jwt access token
    */
    newInvitation: async (username, service) => {
      const cfg = config.get(service);
      if (!cfg) throw new BadRequestError('Invalid service name');
      const { ttl, scope, audience, issuer, url, namespace } = cfg;

      // calculate expiration and issued at time
      const issued = Math.round(new Date().getTime() / 1000);
      const expiration = issued + ttl;

      const uuid = uuidv5(username, namespace);
      const user = `${username}#${uuid.split('-')[0]}`;

      const payload = {
        usr: user,
        sub: uuid,
        iss: issuer,
        iat: issued,
        exp: expiration,
        aud: audience,
        scope: scope,
      }

      const key = await keystore.getKey(service);
      const signedToken = JWT.sign(payload, key);
      return { invitation: `${url}?accessToken=${signedToken}` };
    },

    getPublicKeys: async () => {
      const pubkeys = keystore.getJWKS()
      return pubkeys;
    },

  };
};
