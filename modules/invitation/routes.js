const Router = require('koa-router');

module.exports = ({ invitationService, config, logger, errors }) => {
  const log = logger('module:invitation');
  const { BadRequestError, InternalError } = errors;
  const router = new Router();

  router.get('/invitation/:service', async (ctx) => {
    const { username } = ctx.query;
    const { service } = ctx.params;
    let invitation;

    if (!service)
      throw new BadRequestError('Missing service name');

    if (!username)
      throw new BadRequestError('Missing username');

    try {
      invitation = await invitationService.newInvitation(username, service);
    } catch (e) {
      log.log(e, 'error')
      throw new InternalError('Sorry, that was unexpected...');
    }

    return ctx.ok(invitation);
  });

  router.get('/keystore', async (ctx) => {
    let keystore;

    try {
      keystore = await invitationService.getPublicKeys();
    } catch (e) {
      log.log(e, 'error')
      throw new InternalError('Sorry, that was unexpected...');
    }

    return ctx.ok(keystore);
  });

  return router.routes();
};
