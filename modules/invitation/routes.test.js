const request = require('supertest');
const { app, container } = require('../../app');

describe('authorization routes test', () => {
  const invitationService = container.resolve('invitationService');
  const config = container.resolve('config');

  beforeAll(async () => {
    SERVER = app.listen();
    AGENT = request(SERVER);
  });

  afterAll(async () => {
    SERVER.close();
    await container.dispose();
  });

  test('get an invitation token for a minesweeper service', async () => {
    const response = await AGENT.get('/invitation/minesweeper?username=test')
    expect(response.status).toEqual(200);
    expect(response.type).toEqual('application/json');
    expect(response.body.invitation).toBeTruthy();
  });

  test('get the JWK keys', async () => {
    const response = await AGENT.get('/keystore')
    expect(response.status).toEqual(200);
    expect(response.type).toEqual('application/json');
    expect(response.body.keys).toBeTruthy();
  });
});
