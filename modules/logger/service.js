const logger = require('loglevel');

logger.setLevel(process.env.LOG_LEVEL || 'INFO');
const LEVELS = [
  'debug',
  'info',
  'warn',
  'error',
  'trace',
];

class Logger {
  constructor(procName, appName = 'gs:experience:api') {
    this.appName = appName;
    this.procName = procName || 'unknown';
  }

  log(message, level = 'info') {
    if (!LEVELS.includes(level)) {
      throw new Error(`Invalid log level, it should be one of this [${LEVELS.join(', ')}]`);
    }
    const now = new Date();
    const timestamp = now.toISOString();
    const loginfo = [
      timestamp,
      level,
      this.appName,
      this.procName,
    ];
    logger[level](`[${loginfo.join('] [')}] ${message}`);    
    return true;
  }
}

module.exports = (procName) => {
  const loggerInstance = new Logger(procName);
  return loggerInstance;
};
