const Koa = require('koa');
const mount = require('koa-mount');


module.exports = ({ config, logger }) => {
  const { paths } = config.get('logs');
  const log = logger('module:api');
  const app = new Koa();
  app.use(async (ctx, next) => {
    await next();
    const responseTime = ctx.response.get('X-Response-Time');
    if (paths === '*' || paths.includes(ctx.path)) {
      log.log(`${ctx.method} ${ctx.path} status: ${ctx.status}, response time: ${responseTime}`);
    }
  });

  return mount('/', app);
};
