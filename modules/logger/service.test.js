const { container } = require('../../app');

describe('logger test', () => {
  test('should log a test message', async () => {
    const logger = container.resolve('logger');
    const log = logger('module:logger:test');
    expect(log.log('testing logger module')).toBeTruthy();
  });
});
