FROM node:10.16.3
MAINTAINER Juan M Sanchez.

RUN mkdir app
ADD ./ /app
WORKDIR /app

RUN npm install --loglevel error

EXPOSE 3001

CMD [ "npm", "start" ]
