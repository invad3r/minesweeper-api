require('dotenv').config();
const config = require('config');
const { app, container } = require('./app');

// Load application modules
const logger = require('./modules/logger');

// Set app constants
const PORT = config.get('port');


const log = logger.service('server');
const server = app.listen(PORT).on('error', (err) => {
  log.log(err, 'warn');
});

server.on('close', async () => {
  log.log('server closed');
  await container.dispose();
});

log.log(`Server running on port ${PORT}`);

module.exports = server;
