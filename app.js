require('dotenv').config();

const Koa = require('koa');
const Cors = require('@koa/cors');
const BodyParser = require('koa-bodyparser');
const Helmet = require('koa-helmet');
const respond = require('koa-respond');
const config = require('config');
const awilix = require('awilix');


// Load application modules
const responseTime = require('./modules/response-time');
const status = require('./modules/status');
const logger = require('./modules/logger');
const errorHandler = require('./modules/error-handler');
const swagger = require('./modules/swagger');
const invitation = require('./modules/invitation');
const mongo = require('./modules/mongo-db');
const minesweeper = require('./modules/minesweeper');
const authorization = require('./modules/authorization');

const {
  createContainer,
  asValue,
  asClass,
  asFunction,
  InjectionMode,
} = awilix;

// init koa app and load middlewares
const app = new Koa();

app.use(Helmet());
app.use(Cors());
app.use(BodyParser({
  enableTypes: ['json'],
  jsonLimit: '5mb',
  onerror(err, ctx) {
    ctx.throw('body parse error', 422);
  },
}));

// create dependency injection container
const container = createContainer({
  injectionMode: InjectionMode.PROXY,
});

// registering core dependencies
container.register({
  config: asValue(config),
  logger: asValue(logger.service),
  errors: asValue(errorHandler.errors),
  mongo: asFunction(mongo).singleton(),
});

// registering core middlewares
container.register({
  errorMiddleware: asFunction(errorHandler.middleware),
  loggerMiddleware: asFunction(logger.middleware),
  responseTimeMiddleware: asFunction(responseTime.middleware),
});

// registering application endpoints and services
container.register({
  keystore: asClass(invitation.keystore).singleton(),
  authService: asFunction(authorization.service),
  authMiddleware: asFunction(authorization.middleware),
  invitationRoutes: asFunction(invitation.routes),
  invitationService: asFunction(invitation.service),
  minesweeperRoutes: asFunction(minesweeper.routes),
  minesweeperService: asFunction(minesweeper.service),
  statusRoutes: asFunction(status.routes),
  swaggerRoutes: asFunction(swagger.routes),
});

// loggerMiddleware and responseTimeMiddleware must remain at the begining of the other middlewares
app.use(container.resolve('loggerMiddleware'));
app.use(container.resolve('responseTimeMiddleware'));

app.use(respond());
app.use(container.resolve('errorMiddleware'));
app.use(container.resolve('statusRoutes'));
app.use(container.resolve('swaggerRoutes'));
app.use(container.resolve('invitationRoutes'));
app.use(container.resolve('minesweeperRoutes'));

module.exports = { app, container };
