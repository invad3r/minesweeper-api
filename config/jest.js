module.exports = {
  host: 'http://localhost',
  port: 3000,
  logs: {
    paths: '*', // support an array of strings
  },
  errorHandler: {
    throw: false,
  },
  swagger: {
    file: './swagger-doc.json',
  },
  minesweeper: {
    secret: process.env.MINESWEEPER_SECRET,
    issuer: 'invitation:api:dev',
    audience: 'minesweeper:api:dev',
    namespace: 'd3f5c5d6-1b18-4ec2-a989-881aeea9eaee',
    scope: ['game:save', 'game:load'],
    ttl: 1296000,
    url: 'http://localhost:3001'
  },
  keystore: {
    keys: ['minesweeper']
  }
};
