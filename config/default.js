module.exports = {
  host: 'http://localhost',
  port: 3001,
  logs: {
    paths: '*', // support an array of strings
  },
  errorHandler: {
    throw: false,
  },
  swagger: {
    file: './swagger-doc.json',
    host: 'localhost:3001'
  },
  minesweeper: {
    issuer: 'invitation:api:dev',
    audience: 'minesweeper:api:dev',
    namespace: 'd3f5c5d6-1b18-4ec2-a989-881aeea9eaee',
    scope: ['game:create', 'game:fetch'],
    ttl: 1296000,
    url: 'http://localhost:3000'
  },
  keystore: {
    keys: ['minesweeper'],
  },
  jwks: {
    uri: 'http://localhost:3001/keystore',
  },
  mongo: {
    uri: 'mongodb://localhost:27017/minesweeper',
  }
};
