# Minesweeper API
REST API for the [minesweeper-client](https://gitlab.com/invad3r/minesweeper-client).

try it live on:
[client](https://gitlab.com/invad3r/minesweeper-client).
[api](https://gitlab.com/invad3r/minesweeper-client).

## Installation
requires __node v10.16.3__ or higher.
```
git clone https://gitlab.com/invad3r/article-api
cd minesweeper-api/
npm install
npm start
```

## REST Modules
| Module name        | Description                              | Base path               |
| ------------------ | ---------------------------------------- | ----------------------- |
| status             | returns the API status and uptime        | /status                 |
| swagger            | provides swagger doc for all endpoints   | /swagger                |
| invitation         | provides identity through JWT tokens     | /invitation             |
| minesweeper        | provides game persistence endpoints      | /minesweeper/game       |

## Notes
- For adding user account support I've created an invitation service that returns a JTW token
with the identity as one of his claims.
- For persistence I've used in memory mongo database (easier for testing and deploy).
- For the client app I've used react and redux (first time using redux, please be kind).
- Note that the auth system was intended to be deployed separately, but for the sake of simplicity I've kept it in a single deployment. This module also implements asymmetric public/private keys and a JWK store in order to be easily distributed.
- New invitation URL example http://ec2-52-11-143-171.us-west-2.compute.amazonaws.com:3001/invitation/minesweeper?username=you-user-name. Keep in minds that each request will return a different user due to a uuid being attached to the username to avoid name collisions.

## Test
Tests are implemented with [Jest](https://jestjs.io/en/)
```
npm test
```
